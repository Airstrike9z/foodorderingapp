package com.example.foodorderingapp;

public class Dish {
     String dishName;
     String description;
     String restaurantName;
     double dishSmallSizePrice;
     double dishMediumSizePrice;
     double dishLargeSizePrice;
     int imageResource;
     int dishImageResource;

     String addedToCart = "Added to Cart!";
     public Dish(){

     }

     public Dish(Dish dish){
          this.dishName = dish.dishName;
          this.description = dish.description;
          restaurantName = dish.restaurantName;
          dishSmallSizePrice = dish.dishSmallSizePrice;
          dishMediumSizePrice = dish.dishMediumSizePrice;
          dishLargeSizePrice = dish.dishLargeSizePrice;
          imageResource = dish.imageResource;
          dishImageResource = dish.dishImageResource;
     }
}
