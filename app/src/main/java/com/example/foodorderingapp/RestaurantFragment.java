package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class RestaurantFragment extends Fragment {
    View view;

    RestaurantAdapter restaurantAdapter;

    // XML Views
    RecyclerView foodsRecyclerView;
    Restaurant restaurant;

    public RestaurantFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.restaurant_screen, container, false);
        restaurant = ((MainActivity)getActivity()).getCurrentRestaurant();

        ImageButton to_search_screen_arrow = view.findViewById(R.id.to_back_arrow);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        TextView restaurant_name = view.findViewById(R.id.restaurant_name);
        restaurant_name.setText(restaurant.restaurantName);
        ImageView restaurant_header_image = view.findViewById(R.id.restaurant_header_image);
        restaurant_header_image.setImageResource(restaurant.restaurantHeaderImage);
        TextView restaurant_customers_in_past_month = view.findViewById(R.id.restaurant_customers_in_past_month);
        restaurant_customers_in_past_month.setText(restaurant.customersInPastMonth);
        TextView restaurant_delivery_time = view.findViewById(R.id.restaurant_delivery_time);
        restaurant_delivery_time.setText(restaurant.priceRange);
        TextView restaurant_description = view.findViewById(R.id.restaurant_description);
        restaurant_description.setText(restaurant.description);

        ImageButton savedRestaurantButton = view.findViewById(R.id.restaurant_saved_button);

        if (((MainActivity)getActivity()).checkSaved(restaurant)){
            savedRestaurantButton.setImageResource(R.drawable.filled_heart_icon);
        }
        if (!((MainActivity)getActivity()).checkSaved(restaurant)){
            savedRestaurantButton.setImageResource(R.drawable.unfilled_heart_icon);
        }


        setupRecyclerView();

        to_search_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (((MainActivity)getActivity()).checkSaved(restaurant)) {
//                    ((MainActivity)getActivity()).open_screen(new SearchFragment());
//                }
                ((MainActivity)getActivity()).onBackPressed();
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());

            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        savedRestaurantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity)getActivity()).checkSaved(restaurant)) {
                    ((MainActivity)getActivity()).removeFromSavedRestaurants(restaurant);
                    savedRestaurantButton.setImageResource(R.drawable.unfilled_heart_icon);
                }
                else if (!((MainActivity)getActivity()).checkSaved(restaurant)) {
                    ((MainActivity)getActivity()).addToSavedRestaurants(restaurant);
                    savedRestaurantButton.setImageResource(R.drawable.filled_heart_icon);
                }
            }
        });


        return view;
    }

    void setupRecyclerView() {
        foodsRecyclerView = view.findViewById(R.id.dish_list_view);
        restaurantAdapter = new RestaurantAdapter(getActivity(), restaurant.dishes);
        restaurantAdapter.setMainActivity(((MainActivity)getActivity()));
        foodsRecyclerView.setAdapter(restaurantAdapter);
    }

}