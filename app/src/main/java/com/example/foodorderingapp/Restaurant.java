package com.example.foodorderingapp;

import java.util.ArrayList;

public class Restaurant {
    String restaurantName;
    int imageResourceRestaurant;
    int restaurantHeaderImage;
    int restaurantHomeSavedImage;
    ArrayList<String> restaurantTags;
    int restaurantRating;
    int restaurantPriceRating1;
    int restaurantPriceRating2;
    int restaurantPriceRating3;
    int realRestaurantPriceRating1;
    int realRestaurantPriceRating2;
    int realRestaurantPriceRating3;
    String customersInPastMonth;
    String priceRange;
    String description;

    ArrayList<Dish> dishes;

}
