package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeViewHolder> {

    Context context;
    ArrayList<Restaurant> savedRestaurants;
    MainActivity mainActivity;

    HomeAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> savedRestaurant) {
        this.context = context;
        this.savedRestaurants = savedRestaurant;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_home, parent, false);
        HomeViewHolder viewHolder = new HomeViewHolder(itemView);
        return viewHolder;
    }

//    @Override
//    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
//        Restaurant restaurant = mainActivity.savedRestaurants.get(position);
//        holder.homeSavedRestaurantImage.setImageResource(restaurant.restaurantHomeSavedImage);
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mainActivity.setCurrentRestaurant(restaurant);
//                mainActivity.open_screen(new RestaurantFragment());
//            }
//        });
//    }
//    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        Restaurant restaurant = savedRestaurants.get(position);
        holder.homeSavedRestaurantImage.setImageResource(restaurant.restaurantHomeSavedImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setCurrentRestaurant(restaurant);
                mainActivity.open_screen(new RestaurantFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return savedRestaurants.size();
    }


}
