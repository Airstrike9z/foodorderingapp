package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

public class SearchFragment extends Fragment {
    View view;
    SearchAdapter SearchAdapter;

    // XML Views
    RecyclerView restaurantRecyclerView;
    Restaurant restaurant;
    ArrayList<Restaurant> restaurantList;
    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.search_screen, container, false);

        ImageView to_home_screen_arrow = view.findViewById(R.id.to_home_screen_arrow);
        ImageButton filter_button = view.findViewById(R.id.filter_button);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button_pressed = view.findViewById(R.id.search_button_pressed);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        setupRecyclerView();

        to_home_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        filter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new FiltersFragment());
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });


        return view;
    }

    void setupRecyclerView() {
        restaurantList = ((MainActivity)getActivity()).restaurants;
        restaurantRecyclerView = view.findViewById(R.id.restaurant_list_view);
        SearchAdapter = new SearchAdapter(getActivity(), restaurantList);
        SearchAdapter.setMainActivity(((MainActivity)getActivity()));
        restaurantRecyclerView.setAdapter(SearchAdapter);
    }

}