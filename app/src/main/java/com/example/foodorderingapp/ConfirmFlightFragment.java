package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ConfirmFlightFragment extends Fragment {
    View view;

    public ConfirmFlightFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.confirm_flight_screen, container, false);

        Button yes_confirm_flight_button = view.findViewById(R.id.yes_confirm_flight_button);
        Button no_confirm_flight_button = view.findViewById(R.id.no_confirm_flight_button);

        yes_confirm_flight_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        no_confirm_flight_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new RouteFragment());
            }
        });

        return view;
    }

}