package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SavedAdapter extends RecyclerView.Adapter<SavedViewHolder> {

    Context context;
    ArrayList<Restaurant> savedRestaurants;
    MainActivity mainActivity;

    SavedAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> savedRestaurants) {
        this.context = context;
        this.savedRestaurants = savedRestaurants;
    }

    @NonNull
    @Override
    public SavedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_restaurant, parent, false);
        SavedViewHolder viewHolder = new SavedViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull SavedViewHolder holder, int position) {
        Restaurant restaurant = savedRestaurants.get(position);
        holder.restaurantImage.setImageResource(restaurant.imageResourceRestaurant);
        holder.restaurantName.setText(restaurant.restaurantName);
        String rTags = restaurant.restaurantTags.get(0);
        if (restaurant.restaurantTags.size() > 0){
            for (int i = 1; i < restaurant.restaurantTags.size(); i++){
                rTags += ", " + restaurant.restaurantTags.get(i);
            }
        }
        holder.restaurantTags.setText(rTags);
        holder.restaurantPriceRating1.setImageResource(restaurant.restaurantPriceRating1);
        holder.restaurantPriceRating2.setImageResource(restaurant.restaurantPriceRating2);
        holder.restaurantPriceRating3.setImageResource(restaurant.restaurantPriceRating3);
        holder.realRestaurantPriceRating1.setImageResource(restaurant.realRestaurantPriceRating1);
        holder.realRestaurantPriceRating2.setImageResource(restaurant.realRestaurantPriceRating2);
        holder.realRestaurantPriceRating3.setImageResource(restaurant.realRestaurantPriceRating3);

        if (restaurant.restaurantRating < 3) {
            holder.realRestaurantPriceRating3.setVisibility(View.INVISIBLE);
            if (restaurant.restaurantRating < 2) {
                holder.realRestaurantPriceRating2.setVisibility(View.INVISIBLE);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setCurrentRestaurant(restaurant);
                mainActivity.open_screen(new RestaurantFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return savedRestaurants.size();
    }


}
