package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    Context context;
    ArrayList<CartItems> cartList;
    MainActivity mainActivity;
    Fragment fragment;
    public double subtotal = 0;

    CartAdapter(@NonNull Context context, @NonNull ArrayList<CartItems> cartList, @NonNull Fragment fragment) {
        this.context = context;
        this.cartList = cartList;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_cart, parent, false);
        CartViewHolder viewHolder = new CartViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {

        CartItems cItems = cartList.get(position);
        String name = "(" + cItems.dishes.dishName + ")";
        holder.cartItemName.setText(name);
        String restaurantName = "(" + cItems.dishes.restaurantName + ")";
        holder.cartItemRestaurant.setText(restaurantName);
        String quantity = "(" + Integer.toString(cItems.quantity) + ")";
        holder.cartItemQuantity.setText(quantity);
        String p1 = "$" + String.format ("%.2f", cItems.subtotal);
        holder.cartItemPrice.setText(p1);

        switch (cItems.size) {
            case 1:
                holder.cartItemSize.setText("S");
                break;
            case 2:
                holder.cartItemSize.setText("M");
                break;
            case 3:
                holder.cartItemSize.setText("L");
                break;
        }

        holder.cartItemDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.removeCartItem(cartList,cartList.get(position));
                mainActivity.close_screen(fragment.getFragmentManager());
                mainActivity.open_screen(new CartFragment());
            }
        });

    }

    @Override
    public int getItemCount() {
        if (cartList == null) {
            return 0;
        }
        return cartList.size();
    }

}













































