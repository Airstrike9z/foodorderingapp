package com.example.foodorderingapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class RouteScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_screen);

        Button next_flight_button = findViewById(R.id.next_flight_button);

        next_flight_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_confirm_flight_screen();
            }
        });
    }

    private void open_confirm_flight_screen() {
        Intent open_confirm_flight_screen = new Intent(this, ConfirmFlightScreen.class);
        startActivity(open_confirm_flight_screen);
    }
}
