package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CompletePaymentFragment extends Fragment {
    View view;

    public CompletePaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.completed_payment_screen, container, false);

        Button track_order_button = view.findViewById(R.id.track_order_button);
        Button start_new_route_button = view.findViewById(R.id.start_new_route_button);

        track_order_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new TrackOrderFragment());
                ((MainActivity) getActivity()).order1Ordered = true;
            }
        });

        start_new_route_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new RouteFragment());
            }
        });

        return view;
    }

}