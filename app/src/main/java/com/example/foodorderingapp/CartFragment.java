package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class CartFragment extends Fragment {
    View view;
    CartAdapter cartAdapter;
    RecyclerView cartRecyclerView;
    ArrayList<CartItems> cartList;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.cart_screen, container, false);

        MainActivity mainActivity = ((MainActivity)getActivity());
        TextView see_past_orders_button = view.findViewById(R.id.see_past_orders_button);
        ImageButton to_home_screen_arrow = view.findViewById(R.id.to_home_screen_arrow);
        Button checkout_button = view.findViewById(R.id.checkout_button);
        ImageButton cart_item_delete = view.findViewById(R.id.cart_item_delete);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button_pressed = view.findViewById(R.id.cart_button_pressed);

        TextView cart_subtotal_price = view.findViewById(R.id.cart_subtotal_price);
        TextView cart_tax_price = view.findViewById(R.id.cart_tax_price);
        TextView cart_delivery_price = view.findViewById(R.id.cart_delivery_price);
        TextView cart_total_price = view.findViewById(R.id.cart_total_price);

        setupRecyclerView();

        double realSubtotal = 0;
        double deliveryFee;
        double taxPrice1 = 0;

        ArrayList<CartItems> cartI = mainActivity.cartList;
        for (CartItems c : cartI) {
            realSubtotal += c.subtotal;
        }
        if (realSubtotal == 0) {
            deliveryFee = 0;
        }
        else {
            deliveryFee = 10;
        }

        taxPrice1 = realSubtotal*0.06;

        cart_subtotal_price.setText("$" + String.format ("%.2f", realSubtotal));
        cart_tax_price.setText("$" + String.format ("%.2f", taxPrice1));
        cart_delivery_price.setText("$" + String.format ("%.2f", deliveryFee));
        cart_total_price.setText("$" + String.format ("%.2f", realSubtotal + taxPrice1 + deliveryFee));

        see_past_orders_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new PastOrdersFragment());
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        to_home_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        checkout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new PaymentFragment());
            }
        });

        return view;
    }

    CartAdapter setupRecyclerView() {
        cartList = ((MainActivity)getActivity()).cartList;
        cartRecyclerView = view.findViewById(R.id.cart_list_view);
        cartAdapter = new CartAdapter(getActivity(), cartList, this);
        cartAdapter.setMainActivity(((MainActivity)getActivity()));
        cartRecyclerView.setAdapter(cartAdapter);
        return cartAdapter;
    }

}