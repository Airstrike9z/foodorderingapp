package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    View view;
    ArrayList<Restaurant> savedList;
    RecyclerView homeRecyclerView;
    HomeAdapter HomeAdapter;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.home_screen, container, false);
        MainActivity mainActivity = ((MainActivity)getActivity());

        ImageButton delivery_icon_button_pressed = view.findViewById(R.id.delivery_icon_button_pressed);
        ImageButton delivery_icon_button_unpressed = view.findViewById(R.id.delivery_icon_button_unpressed);
        ImageButton pick_up_icon_button_pressed = view.findViewById(R.id.pick_up_icon_button_pressed);
        ImageButton pick_up_icon_button_unpressed = view.findViewById(R.id.pick_up_icon_button_unpressed);
        Button change_route_button = view.findViewById(R.id.change_route_button);
        ImageButton to_saved_screen_button = view.findViewById(R.id.to_saved_screen_button);
        ImageButton popular_food1_button = view.findViewById(R.id.popular_food1_button);
        ImageButton popular_food2_button = view.findViewById(R.id.popular_food2_button);
        ImageButton popular_food3_button = view.findViewById(R.id.popular_food3_button);
        ImageButton popular_food4_button = view.findViewById(R.id.popular_food4_button);
        ImageButton popular_food5_button = view.findViewById(R.id.popular_food5_button);
        ImageButton popular_food6_button = view.findViewById(R.id.popular_food6_button);
        ImageButton popular_food7_button = view.findViewById(R.id.popular_food7_button);
        ImageButton popular_food8_button = view.findViewById(R.id.popular_food8_button);
        ImageButton homepage_button_pressed = view.findViewById(R.id.homepage_button_pressed);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        setupRecyclerView();


        delivery_icon_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delivery_icon_button_pressed.setVisibility(View.VISIBLE);
                delivery_icon_button_unpressed.setVisibility(View.INVISIBLE);
            }
        });

        delivery_icon_button_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delivery_icon_button_pressed.setVisibility(View.VISIBLE);
                delivery_icon_button_unpressed.setVisibility(View.INVISIBLE);
                pick_up_icon_button_pressed.setVisibility(View.INVISIBLE);
                pick_up_icon_button_unpressed.setVisibility(View.VISIBLE);
            }
        });

        pick_up_icon_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pick_up_icon_button_pressed.setVisibility(View.VISIBLE);
                pick_up_icon_button_unpressed.setVisibility(View.INVISIBLE);
            }
        });

        pick_up_icon_button_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pick_up_icon_button_pressed.setVisibility(View.VISIBLE);
                pick_up_icon_button_unpressed.setVisibility(View.INVISIBLE);
                delivery_icon_button_pressed.setVisibility(View.INVISIBLE);
                delivery_icon_button_unpressed.setVisibility(View.VISIBLE);
            }
        });

        change_route_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new RouteFragment());
            }
        });


        to_saved_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        popular_food1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(0).dishes.get(0));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food2_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(1).dishes.get(1));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food3_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(1).dishes.get(0));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food4_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(0).dishes.get(1));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food5_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(0).dishes.get(3));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food6_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(1).dishes.get(3));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food7_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(1).dishes.get(2));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        popular_food8_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setCurrentDish(((MainActivity) getActivity()).restaurants.get(0).dishes.get(2));
                ((MainActivity)getActivity()).open_screen(new DishFragment());
            }
        });

        homepage_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainActivity.order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        return view;
    }

    void setupRecyclerView() {
        savedList = ((MainActivity)getActivity()).savedRestaurants;
        homeRecyclerView = view.findViewById(R.id.home_list_view);
        HomeAdapter = new HomeAdapter(getActivity(), savedList);
        HomeAdapter.setMainActivity(((MainActivity)getActivity()));
        homeRecyclerView.setAdapter(HomeAdapter);
    }
}