package com.example.foodorderingapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Restaurant currentRestaurant;
    Dish currentDish;
    Boolean order1Ordered = false;

    // Properties
    ArrayList<Restaurant> restaurants = new ArrayList<>();
    ArrayList<CartItems> cartList = new ArrayList<>();
    ArrayList<Restaurant> savedRestaurants = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        open_screen(new HomeFragment());

        populateDataModel();

    }

    void populateDataModel() {
        Restaurant earls = new Restaurant();
        earls.restaurantName = "Earls";
        earls.imageResourceRestaurant = R.drawable.restaurant1;
        earls.restaurantHeaderImage = R.drawable.restaurant_header1;
        earls.restaurantHomeSavedImage = R.drawable.saved_restaurant1;
        earls.customersInPastMonth = "1100";
        earls.priceRange = "$$$";
        earls.description = "Our mission is to deliver irresistible food and drink to every one of our guests.";
        earls.dishes = new ArrayList<Dish>();
        earls.restaurantRating = 3;
        earls.restaurantTags = new ArrayList<String>();
        earls.restaurantTags.add("Fine dining");
        earls.restaurantTags.add("Popular");

        Restaurant riceTales = new Restaurant();
        riceTales.restaurantName = "Rice Tales";
        riceTales.imageResourceRestaurant = R.drawable.restaurant2;
        riceTales.restaurantHeaderImage = R.drawable.restaurant_header2;
        riceTales.restaurantHomeSavedImage = R.drawable.saved_restaurant2;
        riceTales.customersInPastMonth = "900";
        riceTales.priceRange = "$$";
        riceTales.description = "Our goal is to create sustainable yet delicious cuisine for all our patrons.";
        riceTales.dishes = new ArrayList<Dish>();
        riceTales.restaurantRating = 2;
        riceTales.restaurantTags = new ArrayList<String>();
        riceTales.restaurantTags.add("Asian");
        riceTales.restaurantTags.add("Dinner");

        Restaurant denny = new Restaurant();
        denny.restaurantName = "Denny's";
        denny.imageResourceRestaurant = R.drawable.restaurant3;
//        denny.restaurantHeaderImage = R.drawable.restaurant_header2;
//        denny.customersInPastMonth = "900";
//        denny.deliveryTime = "10 mins";
//        denny.description = "Our goal is to create sustainable yet delicious cuisine for all our patrons.";
//        denny.dishes = new ArrayList<Dish>();
        denny.restaurantRating = 1;
        denny.restaurantTags = new ArrayList<String>();
        denny.restaurantTags.add("Breakfast");
        denny.restaurantTags.add("American");

        Restaurant sweetMaple = new Restaurant();
        sweetMaple.restaurantName = "Sweet Maple";
        sweetMaple.imageResourceRestaurant = R.drawable.restaurant4;
//        sweetMaple.restaurantHeaderImage = R.drawable.restaurant_header2;
//        sweetMaple.customersInPastMonth = "900";
//        sweetMaple.deliveryTime = "10 mins";
//        sweetMaple.description = "Our goal is to create sustainable yet delicious cuisine for all our patrons.";
//        sweetMaple.dishes = new ArrayList<Dish>();
        sweetMaple.restaurantRating = 2;
        sweetMaple.restaurantTags = new ArrayList<String>();
        sweetMaple.restaurantTags.add("Dessert");
        sweetMaple.restaurantTags.add("Bakery");

        Restaurant freshSlice = new Restaurant();
        freshSlice.restaurantName = "Freshslice";
        freshSlice.imageResourceRestaurant = R.drawable.restaurant5;
//        freshSlice.restaurantHeaderImage = R.drawable.restaurant_header2;
//        freshSlice.customersInPastMonth = "900";
//        freshSlice.deliveryTime = "10 mins";
//        freshSlice.description = "Our goal is to create sustainable yet delicious cuisine for all our patrons.";
//        freshSlice.dishes = new ArrayList<Dish>();
        freshSlice.restaurantRating = 1;
        freshSlice.restaurantTags = new ArrayList<String>();
        freshSlice.restaurantTags.add("Pizza");
        freshSlice.restaurantTags.add("Italian");

        Restaurant liftBarGrill = new Restaurant();
        liftBarGrill.restaurantName = "Lift Bar Grill";
        liftBarGrill.imageResourceRestaurant = R.drawable.restaurant6;
//        liftBarGrill.restaurantHeaderImage = R.drawable.restaurant_header2;
//        liftBarGrill.customersInPastMonth = "900";
//        liftBarGrill.deliveryTime = "10 mins";
//        liftBarGrill.description = "Our goal is to create sustainable yet delicious cuisine for all our patrons.";
//        liftBarGrill.dishes = new ArrayList<Dish>();
        liftBarGrill.restaurantRating = 3;
        liftBarGrill.restaurantTags = new ArrayList<String>();
        liftBarGrill.restaurantTags.add("Snack");
        liftBarGrill.restaurantTags.add("Bar Food");

        restaurants.add(earls);
        restaurants.add(riceTales);
        restaurants.add(denny);
        restaurants.add(sweetMaple);
        restaurants.add(freshSlice);
        restaurants.add(liftBarGrill);

        savedRestaurants.add(riceTales);

        Dish seafoodPasta = new Dish();
        seafoodPasta.restaurantName = "Earls";
        seafoodPasta.dishName = "Seafood Pasta";
        seafoodPasta.imageResource = R.drawable.restaurant1_food1;
        seafoodPasta.dishImageResource  = R.drawable.dish1;
        seafoodPasta.description = "Freshly made noodles in a creamy and cheesy sauce topped with shrimp, sliced fish and vegetables.";
        seafoodPasta.dishSmallSizePrice = 13.99;
        seafoodPasta.dishMediumSizePrice = 19.99;
        seafoodPasta.dishLargeSizePrice = 24.99;
        earls.dishes.add(seafoodPasta);

        Dish grilledCheese = new Dish();
        grilledCheese.restaurantName = "Earls";
        grilledCheese.dishName = "Grilled Cheese";
        grilledCheese.imageResource = R.drawable.restaurant1_food2;
        grilledCheese.dishImageResource  = R.drawable.dish2;
        grilledCheese.description = "A grill pressed sandwich with three types of cheese inside. Served with sauces on the side.";
        grilledCheese.dishSmallSizePrice = 6.99;
        grilledCheese.dishMediumSizePrice = 9.99;
        grilledCheese.dishLargeSizePrice = 13.99;
        earls.dishes.add(grilledCheese);

        Dish tomatoSoup = new Dish();
        tomatoSoup.restaurantName = "Earls";
        tomatoSoup.dishName = "Tomato Soup";
        tomatoSoup.imageResource = R.drawable.restaurant1_food3;
        tomatoSoup.dishImageResource  = R.drawable.dish3;
        tomatoSoup.description = "Made with fresh Romano tomatoes and heavy cream, topped with chopped almonds and arugula. Served with two toasted pieces of bread.";
        tomatoSoup.dishSmallSizePrice = 4.99;
        tomatoSoup.dishMediumSizePrice = 6.99;
        tomatoSoup.dishLargeSizePrice = 8.99;
        earls.dishes.add(tomatoSoup);

        Dish cheeseCake = new Dish();
        cheeseCake.restaurantName = "Earls";
        cheeseCake.dishName = "Cheesecake";
        cheeseCake.imageResource = R.drawable.restaurant1_food4;
        cheeseCake.dishImageResource = R.drawable.dish4;
        cheeseCake.description = "Creamy and sweet cheesecake topped with icing and sprinkles for the perfect treat to have.";
        cheeseCake.dishSmallSizePrice = 3.99;
        cheeseCake.dishMediumSizePrice = 5.99;
        cheeseCake.dishLargeSizePrice = 10.99;
        earls.dishes.add(cheeseCake);

        Dish eggFriedRice = new Dish();
        eggFriedRice.restaurantName = "Rice Tales";
        eggFriedRice.dishName = "Egg-Fried Rice";
        eggFriedRice.imageResource = R.drawable.restaurant2_food1;
        eggFriedRice.dishImageResource = R.drawable.dish5;
        eggFriedRice.description = "Light yet flavourful egg fried rice topped with dried shrimp and scallions on the side.";
        eggFriedRice.dishSmallSizePrice = 6.99;
        eggFriedRice.dishMediumSizePrice = 10.99;
        eggFriedRice.dishLargeSizePrice = 13.99;
        riceTales.dishes.add(eggFriedRice);

        Dish riceWithBeefAndBeans = new Dish();
        riceWithBeefAndBeans.restaurantName = "Rice Tales";
        riceWithBeefAndBeans.dishName = "Rice w/Beef & Beans";
        riceWithBeefAndBeans.imageResource = R.drawable.restaurant2_food2;
        riceWithBeefAndBeans.dishImageResource = R.drawable.dish6;
        riceWithBeefAndBeans.description = "Perfectly cooked rice served with tender curry beef stir fried with beans and a rich curry sauce on the side.";
        riceWithBeefAndBeans.dishSmallSizePrice = 7.99;
        riceWithBeefAndBeans.dishMediumSizePrice = 11.99;
        riceWithBeefAndBeans.dishLargeSizePrice = 14.99;
        riceTales.dishes.add(riceWithBeefAndBeans);

        Dish chickenStirFry = new Dish();
        chickenStirFry.restaurantName = "Rice Tales";
        chickenStirFry.dishName = "Chicken Stir Fry";
        chickenStirFry.imageResource = R.drawable.restaurant2_food3;
        chickenStirFry.dishImageResource = R.drawable.dish7;
        chickenStirFry.description = "Stir fry chicken topped with refreshing cucumbers and carrots on top of a bed of rice noodles garnished with cilantro.";
        chickenStirFry.dishSmallSizePrice = 8.99;
        chickenStirFry.dishMediumSizePrice = 12.99;
        chickenStirFry.dishLargeSizePrice = 15.99;
        riceTales.dishes.add(chickenStirFry);

        Dish asianRiceNoodles = new Dish();
        asianRiceNoodles.restaurantName = "Rice Tales";
        asianRiceNoodles.dishName = "Asian Rice Noodles";
        asianRiceNoodles.imageResource = R.drawable.restaurant2_food4;
        asianRiceNoodles.dishImageResource = R.drawable.dish8;
        asianRiceNoodles.description = "Rice noodles stir fried with shrimp, red and green peppers served with chopped green onions on the side.";
        asianRiceNoodles.dishSmallSizePrice = 9.99;
        asianRiceNoodles.dishMediumSizePrice = 13.99;
        asianRiceNoodles.dishLargeSizePrice = 16.99;
        riceTales.dishes.add(asianRiceNoodles);
    }

    public void open_screen(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    public void close_screen(FragmentManager fm) {
        fm.popBackStack();
    }

    public void setCurrentDish(Dish dish) {
        currentDish = dish;
    }

    public Dish getCurrentDish() {
        return currentDish;
    }

    public void setCurrentRestaurant(Restaurant restaurant) {
        currentRestaurant = restaurant;
    }

    public Restaurant getCurrentRestaurant() {
        return currentRestaurant;
    }

    public void removeCartItem(ArrayList<CartItems> cList, CartItems removedDish) {
        cList.remove(removedDish);
    }

    public boolean checkSaved(Restaurant restaurant) {
        if (savedRestaurants == null) {
            return false;
        }
        if (savedRestaurants.contains(restaurant)) {
            return true;
        }
        return false;
    }

    public void addToSavedRestaurants(Restaurant restaurant) {
        savedRestaurants.add(restaurant);
    }

    public void removeFromSavedRestaurants(Restaurant restaurant) {
        savedRestaurants.remove(restaurant);
    }
}
