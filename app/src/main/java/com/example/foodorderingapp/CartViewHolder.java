package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartViewHolder extends RecyclerView.ViewHolder{
    public CartViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        cartItemName = itemView.findViewById(R.id.cart_item_name);
        cartItemPrice = itemView.findViewById(R.id.cart_item_price);
        cartItemQuantity = itemView.findViewById(R.id.cart_item_quantity);
        cartItemSize = itemView.findViewById(R.id.cart_item_size);
        cartItemRestaurant = itemView.findViewById(R.id.cart_item_restaurant);
        cartItemDelete = itemView.findViewById(R.id.cart_item_delete);

    }

    View itemView;
    TextView cartItemQuantity;
    TextView cartItemSize;
    TextView cartItemName;
    TextView cartItemPrice;
    TextView cartItemRestaurant;
    ImageButton cartItemDelete;
}
