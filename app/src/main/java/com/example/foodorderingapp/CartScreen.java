package com.example.foodorderingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class CartScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_screen);

        ImageButton to_home_screen_arrow = findViewById(R.id.to_home_screen_arrow);
        Button checkout_button = findViewById(R.id.checkout_button);

        to_home_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_home_screen();
            }
        });

        checkout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_payment_screen();
            }
        });

    }

    private void open_home_screen() {
        finish();
    }

    private void open_payment_screen() {
        Intent open_payment_screen = new Intent(this, PaymentScreen.class);
        startActivity(open_payment_screen);
    }
}