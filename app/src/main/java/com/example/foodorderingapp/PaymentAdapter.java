package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentViewHolder>{

    Context context;
    ArrayList<CartItems> cartList;
    MainActivity mainActivity;

    PaymentAdapter(@NonNull Context context, @NonNull ArrayList<CartItems> cartList) {
        this.context = context;
        this.cartList = cartList;
    }

    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_payment, parent, false);
        PaymentViewHolder viewHolder = new PaymentViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentViewHolder holder, int position) {
        // Taking information from cartList and populating recycler view with specific data from each cartItem

        CartItems cItems = cartList.get(position);
        String name = "(" + cItems.dishes.dishName + ")";
        holder.paymentItemName.setText(name);
        String restaurantName = "(" + cItems.dishes.restaurantName + ")";
        holder.paymentItemRestaurant.setText(restaurantName);
        String quantity = "(" + Integer.toString(cItems.quantity) + ")";
        holder.paymentItemQuantity.setText(quantity);
        String p1 = "$" + String.format ("%.2f", cItems.subtotal);
        holder.paymentItemPrice.setText(p1);

        switch (cItems.size) {
            case 1:
                holder.paymentItemSize.setText("S");
                break;
            case 2:
                holder.paymentItemSize.setText("M");
                break;
            case 3:
                holder.paymentItemSize.setText("L");
                break;
        }

    }

    @Override
    public int getItemCount() {
        if (cartList == null) {
            return 0;
        }
        return cartList.size();
    }

}
