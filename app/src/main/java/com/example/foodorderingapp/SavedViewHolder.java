package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SavedViewHolder extends RecyclerView.ViewHolder {
    public SavedViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        restaurantImage = itemView.findViewById(R.id.restaurantImage);
        restaurantName = itemView.findViewById(R.id.restaurantName);
        restaurantTags = itemView.findViewById(R.id.restaurantTags);
        restaurantPriceRating1 = itemView.findViewById(R.id.restaurantPriceRating1);
        restaurantPriceRating2 = itemView.findViewById(R.id.restaurantPriceRating2);
        restaurantPriceRating3 = itemView.findViewById(R.id.restaurantPriceRating3);
        realRestaurantPriceRating1 = itemView.findViewById(R.id.realRestaurantPriceRating1);
        realRestaurantPriceRating2 = itemView.findViewById(R.id.realRestaurantPriceRating2);
        realRestaurantPriceRating3 = itemView.findViewById(R.id.realRestaurantPriceRating3);
    }

    View itemView;
    ImageView restaurantImage;
    TextView restaurantName;
    TextView restaurantTags;
    ImageView restaurantPriceRating1;
    ImageView restaurantPriceRating2;
    ImageView restaurantPriceRating3;
    ImageView realRestaurantPriceRating1;
    ImageView realRestaurantPriceRating2;
    ImageView realRestaurantPriceRating3;
}
