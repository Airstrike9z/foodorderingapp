package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;

public class Payment2Fragment extends Fragment {
    View view;
    PaymentAdapter paymentAdapter;
    RecyclerView paymentRecyclerView;
    ArrayList<CartItems> cartList;

    public Payment2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.payment2_screen, container, false);

        ImageButton to_payment1_screen_button = view.findViewById(R.id.to_payment1_screen_arrow);
        Button payment_screen_continue_button = view.findViewById(R.id.payment_screen_continue_button);
        ImageButton visa_button_unpressed= view.findViewById(R.id.visa_button_unpressed);
        ImageButton visa_button_pressed= view.findViewById(R.id.visa_button_pressed);
        ImageButton mastercard_button_unpressed= view.findViewById(R.id.mastercard_button_unpressed);
        ImageButton mastercard_button_pressed= view.findViewById(R.id.mastercard_button_pressed);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        to_payment1_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new PaymentFragment());
            }
        });

        payment_screen_continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new Payment3Fragment());
            }
        });

        visa_button_unpressed.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 visa_button_unpressed.setVisibility(View.INVISIBLE);
                 visa_button_pressed.setVisibility(View.VISIBLE);
                 mastercard_button_unpressed.setVisibility(View.VISIBLE);
                 mastercard_button_pressed.setVisibility(View.INVISIBLE);
             }
         });

        mastercard_button_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mastercard_button_unpressed.setVisibility(View.INVISIBLE);
                mastercard_button_pressed.setVisibility(View.VISIBLE);
                visa_button_unpressed.setVisibility(View.VISIBLE);
                visa_button_pressed.setVisibility(View.INVISIBLE);
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new CartFragment());
            }
        });

        return view;
    }

}
