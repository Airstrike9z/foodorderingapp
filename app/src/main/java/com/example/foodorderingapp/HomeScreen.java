package com.example.foodorderingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        ImageButton to_saved_screen_button = findViewById(R.id.to_saved_screen_button);
        ImageButton homepage_button = findViewById(R.id.homepage_button);
        ImageButton saved_button = findViewById(R.id.saved_button);
        ImageButton search_button = findViewById(R.id.search_button);
        ImageButton location_button = findViewById(R.id.location_button);
        ImageButton cart_button = findViewById(R.id.cart_button);


        to_saved_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_saved_screen();
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_home_screen();
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_route_screen();
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_search_screen();
            }
        });
//
//        location_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                open_location_screen();
//            }
//        });
//
        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_cart_screen();
            }
        });
    }


    private void open_saved_screen() {
        Intent open_saved_screen = new Intent(this, SavedScreen.class);
        startActivity(open_saved_screen);
    }

    private void open_home_screen() {
        Intent open_home_screen = new Intent(this, HomeScreen.class);
        startActivity(open_home_screen);
    }

    private void open_route_screen() {
        Intent open_route_screen = new Intent(this, RouteScreen.class);
        startActivity(open_route_screen);
    }

    private void open_search_screen() {
        Intent open_search_screen = new Intent(this, SearchScreen.class);
        startActivity(open_search_screen);
    }

    private void open_cart_screen() {
        Intent open_cart_screen = new Intent(this, CartScreen.class);
        startActivity(open_cart_screen);
    }
}
