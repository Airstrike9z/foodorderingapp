package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PaymentFragment extends Fragment {
    View view;
    PaymentAdapter paymentAdapter;
    RecyclerView paymentRecyclerView;
    ArrayList<CartItems> cartList;

    public PaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.payment_screen, container, false);
        MainActivity mainActivity = ((MainActivity)getActivity());

        Button payment_screen_continue_button = view.findViewById(R.id.payment_screen_continue_button);
        ImageButton to_cart_screen_button = view.findViewById(R.id.to_cart_screen_arrow);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);
        TextView payment_subtotal_price = view.findViewById(R.id.payment_subtotal_price);
        TextView payment_tax_price = view.findViewById(R.id.payment_tax_price);
        TextView payment_delivery_price = view.findViewById(R.id.payment_delivery_price);
        TextView payment_total_price = view.findViewById(R.id.payment_total_price);

        setupRecyclerView();

        double realSubtotal = 0;
        double deliveryFee;
        double taxPrice1;

        ArrayList<CartItems> cartI = mainActivity.cartList;
        for (CartItems c : cartI) {
            realSubtotal += c.subtotal;
        }
        if (realSubtotal == 0) {
            deliveryFee = 0;
        }
        else {
            deliveryFee = 10;
        }

        taxPrice1 = realSubtotal*0.06;

        payment_subtotal_price.setText("$" + String.format ("%.2f", realSubtotal));
        payment_tax_price.setText("$" + String.format ("%.2f", taxPrice1));
        payment_delivery_price.setText("$" + String.format ("%.2f", deliveryFee));
        payment_total_price.setText("$" + String.format ("%.2f", realSubtotal + taxPrice1 + deliveryFee));

        payment_screen_continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new Payment2Fragment());
            }
        });

        to_cart_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        return view;
    }

    void setupRecyclerView() {
        cartList = ((MainActivity)getActivity()).cartList;
        paymentRecyclerView = view.findViewById(R.id.payment_list_view);
        paymentAdapter = new PaymentAdapter(getActivity(), cartList);
        paymentAdapter.setMainActivity(((MainActivity)getActivity()));
        paymentRecyclerView.setAdapter(paymentAdapter);
    }

}