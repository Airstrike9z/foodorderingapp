package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RestaurantViewHolder extends RecyclerView.ViewHolder {
    public RestaurantViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        restaurantFoodImage = itemView.findViewById(R.id.restaurantFoodImage);
        restaurantHeaderImage = itemView.findViewById(R.id.restaurant_header_image);
        foodName = itemView.findViewById(R.id.foodName);
        foodPrice = itemView.findViewById(R.id.foodPrice);
        savedButton = itemView.findViewById(R.id.restaurant_saved_button);
    }

    View itemView;
    ImageView restaurantFoodImage;
    ImageView restaurantHeaderImage;
    TextView foodName;
    TextView foodPrice;
    ImageButton savedButton;
}
