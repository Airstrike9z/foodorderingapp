package com.example.foodorderingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class RestaurantScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_screen);

        ImageButton back_to_screen_arrow = findViewById(R.id.to_back_arrow);

        back_to_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return_to_previous_screen();
            }
        });

    }

    private void return_to_previous_screen() {
        finish();
    }
    
}