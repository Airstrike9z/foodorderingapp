package com.example.foodorderingapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ConfirmFlightScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_flight_screen);

        Button yes_confirm_flight_button = findViewById(R.id.yes_confirm_flight_button);
        Button no_confirm_flight_button = findViewById(R.id.no_confirm_flight_button);


        yes_confirm_flight_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_home_screen();
            }
        });

        no_confirm_flight_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_route_screen();
            }
        });
    }


    private void open_home_screen() {
        Intent open_home_screen = new Intent(this, MainActivity.class);
        startActivity(open_home_screen);
    }

    private void open_route_screen() {
        finish();
    }
}