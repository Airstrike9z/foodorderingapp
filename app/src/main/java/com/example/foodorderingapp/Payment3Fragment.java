package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

public class Payment3Fragment extends Fragment {
    View view;
    PaymentAdapter paymentAdapter;
    RecyclerView paymentRecyclerView;
    ArrayList<CartItems> cartList;

    public Payment3Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.payment3_screen, container, false);

        ImageButton to_payment2_screen_button = view.findViewById(R.id.to_payment2_screen_arrow);
        ImageButton check_box_button1_unpressed= view.findViewById(R.id.check_box_button1_unpressed);
        ImageButton check_box_button1_pressed= view.findViewById(R.id.check_box_button1_pressed);
        ImageView background_1 = view.findViewById(R.id.background_1);
        ImageButton check_box_button2_unpressed= view.findViewById(R.id.check_box_button2_unpressed);
        ImageButton check_box_button2_pressed= view.findViewById(R.id.check_box_button2_pressed);
        ImageView background_2 = view.findViewById(R.id.background_2);
        ImageButton check_box_button3_unpressed= view.findViewById(R.id.check_box_button3_unpressed);
        ImageButton check_box_button3_pressed= view.findViewById(R.id.check_box_button3_pressed);
        ImageView background_3 = view.findViewById(R.id.background_3);
        ImageButton check_box_button4_unpressed= view.findViewById(R.id.check_box_button4_unpressed);
        ImageButton check_box_button4_pressed= view.findViewById(R.id.check_box_button4_pressed);
        ImageView background_4 = view.findViewById(R.id.background_4);
        Button complete_payment_button = view.findViewById(R.id.complete_payment_button);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        to_payment2_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new Payment2Fragment());
            }
        });

        check_box_button1_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_box_button1_unpressed.setVisibility(View.INVISIBLE);
                check_box_button1_pressed.setVisibility(View.VISIBLE);
                background_1.setVisibility(View.VISIBLE);
                check_box_button2_unpressed.setVisibility(View.VISIBLE);
                check_box_button2_pressed.setVisibility(View.INVISIBLE);
                background_2.setVisibility(View.INVISIBLE);
                check_box_button3_unpressed.setVisibility(View.VISIBLE);
                check_box_button3_pressed.setVisibility(View.INVISIBLE);
                background_3.setVisibility(View.INVISIBLE);
                check_box_button4_unpressed.setVisibility(View.VISIBLE);
                check_box_button4_pressed.setVisibility(View.INVISIBLE);
                background_4.setVisibility(View.INVISIBLE);
            }
        });

        check_box_button2_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_box_button1_unpressed.setVisibility(View.VISIBLE);
                check_box_button1_pressed.setVisibility(View.INVISIBLE);
                background_1.setVisibility(View.INVISIBLE);
                check_box_button2_unpressed.setVisibility(View.INVISIBLE);
                check_box_button2_pressed.setVisibility(View.VISIBLE);
                background_2.setVisibility(View.VISIBLE);
                check_box_button3_unpressed.setVisibility(View.VISIBLE);
                check_box_button3_pressed.setVisibility(View.INVISIBLE);
                background_3.setVisibility(View.INVISIBLE);
                check_box_button4_unpressed.setVisibility(View.VISIBLE);
                check_box_button4_pressed.setVisibility(View.INVISIBLE);
                background_4.setVisibility(View.INVISIBLE);
            }
        });

        check_box_button3_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_box_button1_unpressed.setVisibility(View.VISIBLE);
                check_box_button1_pressed.setVisibility(View.INVISIBLE);
                background_1.setVisibility(View.INVISIBLE);
                check_box_button2_unpressed.setVisibility(View.VISIBLE);
                check_box_button2_pressed.setVisibility(View.INVISIBLE);
                background_2.setVisibility(View.INVISIBLE);
                check_box_button3_unpressed.setVisibility(View.INVISIBLE);
                check_box_button3_pressed.setVisibility(View.VISIBLE);
                background_3.setVisibility(View.VISIBLE);
                check_box_button4_unpressed.setVisibility(View.VISIBLE);
                check_box_button4_pressed.setVisibility(View.INVISIBLE);
                background_4.setVisibility(View.INVISIBLE);
            }
        });

        check_box_button4_unpressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_box_button1_unpressed.setVisibility(View.VISIBLE);
                check_box_button1_pressed.setVisibility(View.INVISIBLE);
                background_1.setVisibility(View.INVISIBLE);
                check_box_button2_unpressed.setVisibility(View.VISIBLE);
                check_box_button2_pressed.setVisibility(View.INVISIBLE);
                background_2.setVisibility(View.INVISIBLE);
                check_box_button3_unpressed.setVisibility(View.VISIBLE);
                check_box_button3_pressed.setVisibility(View.INVISIBLE);
                background_3.setVisibility(View.INVISIBLE);
                check_box_button4_unpressed.setVisibility(View.INVISIBLE);
                check_box_button4_pressed.setVisibility(View.VISIBLE);
                background_4.setVisibility(View.VISIBLE);
            }
        });

        complete_payment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new CompletePaymentFragment());
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).open_screen(new CartFragment());
            }
        });

        return view;
    }

}
