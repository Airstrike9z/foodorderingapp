package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeViewHolder extends RecyclerView.ViewHolder{
    public HomeViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        homeSavedRestaurantImage = itemView.findViewById(R.id.homeSavedRestaurantImage);
    }

    View itemView;
    ImageView homeSavedRestaurantImage;
}
