package com.example.foodorderingapp;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PaymentViewHolder extends RecyclerView.ViewHolder{
    public PaymentViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        paymentItemName = itemView.findViewById(R.id.payment_item_name);
        paymentItemPrice = itemView.findViewById(R.id.payment_item_price);
        paymentItemQuantity = itemView.findViewById(R.id.payment_item_quantity);
        paymentItemSize = itemView.findViewById(R.id.payment_item_size);
        paymentItemRestaurant = itemView.findViewById(R.id.payment_item_restaurant);
    }

    View itemView;
    TextView paymentItemQuantity;
    TextView paymentItemSize;
    TextView paymentItemName;
    TextView paymentItemPrice;
    TextView paymentItemRestaurant;
}
