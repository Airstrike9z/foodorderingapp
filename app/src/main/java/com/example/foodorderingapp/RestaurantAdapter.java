package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

    Context context;
    ArrayList<Dish> dishes;
    MainActivity mainActivity;

    RestaurantAdapter(@NonNull Context context, @NonNull ArrayList<Dish> dishes) {
        this.context = context;
        this.dishes = dishes;
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_food, parent, false);
        RestaurantViewHolder viewHolder = new RestaurantViewHolder(itemView);
        return viewHolder;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        Dish dish = dishes.get(position);
        holder.restaurantFoodImage.setImageResource(dish.imageResource);
        holder.foodName.setText(dish.dishName);
        String dishCost = "$" + Double.toString(dish.dishMediumSizePrice);
        holder.foodPrice.setText(dishCost);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setCurrentDish(dish);
                mainActivity.open_screen(new DishFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (dishes == null) {
            return 0;
        }
        return dishes.size();
    }
}
