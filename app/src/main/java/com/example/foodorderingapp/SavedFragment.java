package com.example.foodorderingapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class SavedFragment extends Fragment {
    View view;
    SavedAdapter SavedAdapter;

    RecyclerView savedRecyclerView;
    Restaurant restaurant;
    ArrayList<Restaurant> savedList;
    public SavedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.saved_screen, container, false);

        ImageButton to_home_screen_arrow = view.findViewById(R.id.to_home_screen_arrow);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button_pressed = view.findViewById(R.id.saved_button_pressed);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);

        setupRecyclerView();

        to_home_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new HomeFragment());
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new HomeFragment());
            }
        });

        saved_button_pressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen (new CartFragment());
            }
        });

        return view;
    }

    void setupRecyclerView() {
        savedList = ((MainActivity)getActivity()).savedRestaurants;
        savedRecyclerView = view.findViewById(R.id.saved_list_view);
        SavedAdapter = new SavedAdapter(getActivity(), savedList);
        SavedAdapter.setMainActivity(((MainActivity)getActivity()));
        savedRecyclerView.setAdapter(SavedAdapter);
    }
}