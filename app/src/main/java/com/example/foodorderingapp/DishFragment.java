package com.example.foodorderingapp;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DishFragment extends Fragment {
    View view;
    Dish dish;
    CartItems cartItem = new CartItems();
    String newPrice;
    int tempPrice = 1;

    boolean smallSizeClicked = false;
    boolean mediumSizeClicked = false;
    boolean largeSizeClicked = false;

    public DishFragment() {
        // Required empty public constructor
    }

    public boolean checkIfSizeSelected() {
        if (!smallSizeClicked && !mediumSizeClicked && !largeSizeClicked) {
            return false;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.item_dish, container, false);
        dish = ((MainActivity)getActivity()).getCurrentDish();
        cartItem.quantity = 1;
        cartItem.dishes = dish;
        tempPrice = 1;

        ImageButton to_food_screen_arrow = view.findViewById(R.id.to_food_screen_arrow);
        ImageButton homepage_button = view.findViewById(R.id.homepage_button);
        ImageButton saved_button = view.findViewById(R.id.saved_button);
        ImageButton search_button = view.findViewById(R.id.search_button);
        ImageButton location_button = view.findViewById(R.id.location_button);
        ImageButton cart_button = view.findViewById(R.id.cart_button);
        Button small_size_button = view.findViewById(R.id.small_size_button);
        Button medium_size_button = view.findViewById(R.id.medium_size_button);
        Button large_size_button = view.findViewById(R.id.large_size_button);
        ImageButton quantity_plus_button = view.findViewById(R.id.add_quantity_button);
        ImageButton quantity_subtract_button = view.findViewById(R.id.subtract_quantity_button);
        Button add_to_cart_button = view.findViewById(R.id.add_to_cart_button);
        TextView dish_name = view.findViewById(R.id.dish_name);
        dish_name.setText(dish.dishName);
        ImageView restaurant_dish_image = view.findViewById(R.id.restaurant_dish_image);
        restaurant_dish_image.setImageResource(dish.dishImageResource);
        TextView restaurant_dish_description = view.findViewById(R.id.restaurant_dish_description);
        restaurant_dish_description.setText(dish.description);
        TextView dish_small_size_price = view.findViewById(R.id.dish_small_size_price);
        dish_small_size_price.setText("$" + Double.toString(dish.dishSmallSizePrice));
        TextView dish_medium_size_price = view.findViewById(R.id.dish_medium_size_price);
        dish_medium_size_price.setText("$" + Double.toString(dish.dishMediumSizePrice));
        TextView dish_large_size_price = view.findViewById(R.id.dish_large_size_price);
        dish_large_size_price.setText("$" + Double.toString(dish.dishLargeSizePrice));
        TextView dish_quantity_number = view.findViewById(R.id.dish_quantity_number);

        to_food_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).onBackPressed();
            }
        });

        homepage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new HomeFragment());
            }
        });

        saved_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SavedFragment());
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new SearchFragment());
            }
        });

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).order1Ordered) {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderFragment());
                }
                else {
                    ((MainActivity)getActivity()).open_screen(new TrackOrderEmptyFragment());
                }
            }
        });

        cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).open_screen(new CartFragment());
            }
        });

        small_size_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 cartItem.size = 1;
                 smallSizeClicked = true;
                 mediumSizeClicked = false;
                 largeSizeClicked = false;
                 System.out.println("small size button = true, medium size button = false, large size button = false");
                 small_size_button.setBackgroundResource(R.drawable.rounded_rectangle9);
                 medium_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
                 large_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
            }
        });

        medium_size_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartItem.size = 2;
                smallSizeClicked = false;
                mediumSizeClicked = true;
                largeSizeClicked = false;
                System.out.println("small size button = false, medium size button = true, large size button = false");
                small_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
                medium_size_button.setBackgroundResource(R.drawable.rounded_rectangle9);
                large_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
            }
        });

        large_size_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartItem.size = 3;
                smallSizeClicked = false;
                mediumSizeClicked = false;
                largeSizeClicked = true;
                System.out.println("small size button = false, medium size button = false, large size button = true");
                small_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
                medium_size_button.setBackgroundResource(R.drawable.rounded_rectangle3);
                large_size_button.setBackgroundResource(R.drawable.rounded_rectangle9);
            }
        });

        quantity_plus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempPrice += 1;
                String newNum = Integer.toString(tempPrice);
                dish_quantity_number.setText(newNum);
            }
        });

        quantity_subtract_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempPrice-1 > 0){
                    tempPrice -= 1;
                }
                String newNum = Integer.toString(tempPrice);
                dish_quantity_number.setText(newNum);
            }
        });

        add_to_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfSizeSelected()) {
                    Integer t = new Integer(tempPrice);
                    cartItem.quantity = t;
                    double price = 0;
                    if (cartItem.size == 1) {
                        price = cartItem.dishes.dishSmallSizePrice;
                    }
                    if (cartItem.size == 2) {
                        price = cartItem.dishes.dishMediumSizePrice;
                    }
                    if (cartItem.size == 3) {
                        price = cartItem.dishes.dishLargeSizePrice;
                    }
                    cartItem.subtotal = price * cartItem.quantity;

                    ((MainActivity) getActivity()).cartList.add(new CartItems(cartItem));
                    add_to_cart_button.setText(dish.addedToCart);
                }
                else {
                    openAlertScreen(getActivity());
                }
            }
        });

        return view;
    }

    private void openAlertScreen(Context context) {
        new AlertDialog.Builder(context)
            .setTitle("Cannot Add To Cart!")
            .setMessage("Please select a dish size before adding to cart!")
            .setPositiveButton("ok", null)
            .setIcon(R.drawable.close_icon)
            .show();
    }

}