package com.example.foodorderingapp;

public class CartItems {
    int quantity;
    int size;
    Dish dishes;
    double subtotal;

    public CartItems(){

    }

    public CartItems(CartItems cartItems){
        quantity = cartItems.quantity;
        size = cartItems.size;
        dishes = new Dish(cartItems.dishes);
        subtotal = cartItems.subtotal;
    }
}
