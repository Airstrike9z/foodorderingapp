package com.example.foodorderingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class SearchScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_screen);

        ImageButton back_to_home_screen_arrow = findViewById(R.id.to_home_screen_arrow);

        back_to_home_screen_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_home_screen();
            }
        });

    }

    private void open_home_screen() {
        Intent open_home_screen = new Intent(this, HomeScreen.class);
        startActivity(open_home_screen);
    }

    private void open_restaurant_screen() {
        Intent open_restaurant_screen = new Intent(this, RestaurantScreen.class);
        startActivity(open_restaurant_screen);
    }
}